<?php
/**
 * Routes for Web services Version 1.0
 */
Route::group(['prefix' => 'v1', 'namespace' => 'WebServices'], function () {
	/**
	 * User Authentication Routes
	 */
	Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
		Route::post('register', 'AuthController@register');
		Route::post('login', 'AuthController@login');
	});
});
