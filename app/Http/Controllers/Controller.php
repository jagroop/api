<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController {
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	/**
	 * Return Json response
	 * @param  array  $response Response array
	 * @return json
	 */
	public function response($response) {
		return response()->json($response);
		header('Content-Type: application/json');
		echo json_encode($response);
		die;
	}
	/**
	 * if operation successfully performed
	 * @param  int $msg  Message t obe displayed
	 * @param  array  $data data to return with success message
	 * @return callback
	 */
	public function success($msg = "Success", $data = array()) {
		return $this->response(array('code' => 200, 'success' => true, 'msg' => $msg, 'data' => $data));
	}
	/**
	 * If operation was'nt performed successfully
	 * @param  string $error Error Message
	 * @return callback
	 */
	public function error($error = "Something went Wrong", $messages = array()) {
		return $this->response(array('code' => 400, 'success' => false, 'error' => $error, 'messages' => $messages));
	}
}
