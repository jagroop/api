<?php

namespace App\Http\Controllers\WebServices\Auth;
use App\Http\Controllers\Controller;
// use App\Mail\UserRegistered;
use App\Models\User;
use Carbon\Carbon;
use DB;
use Hash;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Mail;
use Validator;

class AuthController extends Controller {

	/**
	 * Register a User
	 * @return [type] [description]
	 */
	public function register(Request $request) {
		$requestData = request(['name', 'email', 'password']);

		$validator = Validator::make($request->all(), [
			'name' => 'required|unique:users,user_username',
			'email' => 'required|email|unique:users,user_email',
			'password' => 'required',
		]);

		if ($validator->fails()) {
			return $this->error("Validation Failed.", $validator->errors());
		}
		$move = false;
		$photoName = 'avatar.jpg';
		if ($request->hasFile('user_photo')) {
			$photoName = time() . '.' . $request->user_photo->getClientOriginalExtension();
			$move = $request->user_photo->move(public_path('uploads/users'), $photoName);
		}

		$user = User::create([
			'user_username' => $request['name'],
			'user_email' => $request['email'],
			'user_password' => bcrypt($request['password']),
			'user_profile_picture' => ($move) ? $photoName : 'avatar.jpg',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		if (count($user)) {
			$user->user_profile_picture = asset('uploads/users') . '/' . $user->user_profile_picture;
			$to = $user;
			$to->email = $user->user_email;
			// Mail::to($to)
			// 	->queue(new UserRegistered($user));
		}

		return (count($user)) ? $this->success('User has been registered successfully.', $user) : $this->error('An error occurred please try again.');

	}

	/**
	 * Login A User
	 * @return JSON
	 */
	public function login() {

		$request = request(['email', 'password']);

		$validator = Validator::make($request, [
			'email' => 'required|email',
			'password' => 'required',
		]);

		if ($validator->fails()) {
			return $this->error("Validation Failed.", $validator->errors());
		}

		$user = DB::table('users')->where('user_email', $request['email'])->first();
		if (count($user)) {
			// Check Password hash
			if (Hash::check($request['password'], $user->user_password)) {
				$user->user_profile_picture = asset('uploads/users') . '/' . $user->user_profile_picture;
				unset($user->user_password);
				return $this->success('User successfully logged in.', $user);
			}
		}

		return $this->error('Wrong Email Or Password.');
	}
}
